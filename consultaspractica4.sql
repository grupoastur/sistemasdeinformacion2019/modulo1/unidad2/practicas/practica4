﻿USE practica4;
-- 1 Averigua el dni de todos los clientes
SELECT c.dni FROM cliente c; 

-- 2 Consulta todos los datos de todos los programas
SELECT * FROM programa p; 

-- 3 Obtén un listado con los nombres de todos los programas
SELECT DISTINCT p.nombre FROM programa p; 

-- 4 Genera una lista con todos los comercios
SELECT DISTINCT c.nombre FROM comercio c; 

-- 5 Genera una lista de las ciudades con establecimientos donde se venden programas, sin que aparezcan valores duplicados (utiliza distinct)
SELECT DISTINCT c.ciudad FROM comercio c JOIN distribuye d ON c.cif = d.cif; 

-- 6 Obtén una lista con los nombres de programas, sin que aparezcan valores duplicados (utiliza distinct)
SELECT DISTINCT p.nombre FROM programa p; 

-- 7 Obtén el dni + 4 de todos los clientes
SELECT c.dni+4 FROM cliente c; 
   
-- 8 Haz un listado con los códigos de los programas muliplicados por 7
SELECT DISTINCT p.codigo*7 FROM programa p; 

-- 9 ¿Cuales son los programas cuyo código es inferior o igual a 10?
SELECT DISTINCT p.nombre, p.codigo FROM programa p WHERE p.codigo<=10; 

-- 10 ¿Cual es el programa cuyo código es 11?
SELECT p.nombre FROM programa p WHERE p.codigo=11; 

-- 11 ¿Qué fabricantes son de Estados Unidos?
SELECT f.nombre FROM fabricante f WHERE f.pais='estados unidos'; 

-- 12 ¿Cuáles son los fabricantes no españoles?
SELECT f.nombre FROM fabricante f WHERE f.pais<>'españa'; 

-- 13 Obtén un listado con los códigos de las distintas versiones de Windows
SELECT p.codigo, p.version FROM programa p WHERE p.nombre='windows'; 

-- 14 ¿En que ciudades comercializa programas El Corte Inglés?
SELECT c.ciudad, c.nombre  FROM comercio c WHERE c.nombre='el corte ingles';
   
-- 15 ¿Qué otros comercios hay, además de El Corte Inglés?
SELECT c.nombre FROM comercio c WHERE c.nombre<>'el corte ingles'; 

-- 16 Genera una lista con los códigos de las distintas versiones de Windows y Acces. Utiliza el operador IN
SELECT p.codigo, p.version, p.nombre FROM programa p WHERE p.nombre IN('windows','access');   
  
-- 17 Obtén un listado que incluya los nombres de los clientes de edades comprendidas entre 10 y 25 y de los mayores de 50 años.
-- Da una solución con BETWEEN y otra sin BETWEEN
SELECT c.nombre, c.edad,  FROM cliente c WHERE c.edad BETWEEN 10 AND 25 OR c.edad >50;  
 SELECT c.nombre, c.edad FROM cliente c WHERE c.edad >10 AND c.edad <25 OR c.edad >50;
      
-- 18 Saca un listado con los comercios de SEVILLA y MADRID. No se admiten valores duplicados.
  SELECT DISTINCT c.nombre FROM comercio c WHERE c.ciudad='sevilla' OR c.ciudad='madrid'; 
  
-- 19 ¿Qué clientes terminan su nombre en la letra "o"?
  SELECT * FROM cliente c WHERE c.nombre LIKE '%o'; 
  
-- 20 ¿Qué clientes terminan su nombre en la letra "o" y, además, son mayores de 30 años?
  SELECT * FROM cliente c WHERE c.nombre LIKE '%o' AND c.edad>30;  
  
-- 21 Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i, 
-- o cuyo nombre comience por una A o por una W.
  SELECT * FROM programa p WHERE p.version LIKE '%i' OR p.nombre LIKE 'a%' OR p.nombre LIKE 'w%';

-- 22 Obtén un listado en el que aparezcan los programas cuya versión finalice por una letra i,
-- o cuyo nombre no comience por una A y termine por una S.
SELECT * FROM programa p WHERE p.version LIKE '%i' OR p.nombre NOT LIKE 'A%' AND p.nombre NOT LIKE '%s';
  
-- 23 Obtén un listado en el que aparezcan los  programas cuya versión finalice por una letra i, y cuyo nombre no comience por una A.
SELECT * FROM programa p WHERE p.version LIKE '%i' OR p.nombre NOT LIKE 'a%'; 

-- 24 Obtén una lista de empresas por orden alfabético ascendente.
SELECT DISTINCT c.nombre FROM comercio c ORDER BY c.nombre ASC; 

-- 25 Genera un listado de empresas por orden alfabético descendente.
SELECT DISTINCT c.nombre FROM comercio c ORDER BY c.nombre DESC;

-- 26 Obtén un listado de programas por orden de versión
 SELECT DISTINCT p.nombre, p.version FROM  programa p ORDER BY p.version ; 
       
-- 27 Genera un litado de los programas que desarroya Oracle
SELECT p.codigo, p.nombre, p.version FROM desarrolla d JOIN programa p ON d.codigo = p.codigo JOIN fabricante f ON d.id_fab = f.id_fab WHERE f.nombre='Oracle';   

-- 28 ¿Qué comercios destribuyen Windows?
SELECT c.nombre FROM programa p JOIN distribuye d ON p.codigo = d.codigo JOIN comercio c ON d.cif = c.cif WHERE p.nombre='windows'; 

-- 29 Genera un litado de los programas y cantidades que se han distribuido a El Corte Inglés de Madrid
SELECT p.nombre, d.cantidad FROM comercio c JOIN distribuye d ON c.cif = d.cif JOIN programa p ON d.codigo = p.codigo WHERE c.nombre='el corte ingles';  

-- 30 ¿Qué fabricante ha desarrollado Freddy Hardest?
  SELECT f.nombre FROM fabricante f JOIN desarrolla d ON f.id_fab = d.id_fab JOIN programa p ON d.codigo = p.codigo WHERE p.nombre='freddy hardest';
  
-- 31 selecciona el nombre de los programas que se registran por internet
  SELECT p.nombre FROM programa p JOIN registra r ON p.codigo = r.codigo WHERE r.medio='internet';     
    
-- 32 selecciona el nombre de las personas que se registran por internet
   SELECT c.nombre FROM cliente c JOIN registra r ON c.dni = r.dni WHERE r.medio='internet';  
    
-- 33 ¿que medios ha utilizado para registrarse pepe perez?
 SELECT r.medio FROM cliente c JOIN registra r ON c.dni = r.dni WHERE c.nombre='pepe perez';    
    
-- 34 ¿que usuarios han optado por internet como medio de registro?
 SELECT r.dni FROM registra r WHERE r.medio='internet'; 
     
-- 35 ¿que programas han recibido registros por targeta postal?
 SELECT p.nombre FROM programa p JOIN registra r ON p.codigo = r.codigo WHERE r.medio='tarjeta postal';    
    
-- 36 ¿en que localidades se han vendido productos que se han registrado por internet?
SELECT c.ciudad FROM comercio c JOIN registra r ON c.cif = r.cif WHERE r.medio='internet';     
    
-- 37 obten un listado de los nombres de las personas que se han  registrado por internet, 
-- junto al nombre de los programas para los que ha efectuado el registro
 SELECT DISTINCT p.nombre, c.nombre FROM registra r JOIN programa p ON r.codigo = p.codigo 
  JOIN cliente c ON r.dni = c.dni WHERE r.medio='internet';      

-- 38 genera un listado en el que aparezca cada cliente junto al programa que ha registrado, 
-- el medio con el que lo ha hecho y el comercio en el que lo ha adquirido         
SELECT r.dni, p.nombre, r.medio, r.cif FROM programa p JOIN registra r ON p.codigo = r.codigo;  

-- 39 genera un listado con las cicudades en las que se pueden obtener los productos de oracle
SELECT * FROM comercio c JOIN fabricante f ON c.nombre = f.nombre WHERE f.nombre='oracle'; 

-- 40 obten el nombre de los usuarios que han registrado access xp
SELECT DISTINCT c.nombre No_hay_usuarios FROM registra r JOIN programa p ON r.codigo = p.codigo JOIN cliente c ON r.dni = c.dni
 WHERE p.nombre='access' AND p.version='xp'; 

-- 41 nombre de aquellos fabricantes cuyo pais es el mismo que oracle. (subconsulta)
SELECT f.pais FROM fabricante f WHERE f.nombre='oracle';
SELECT f.nombre FROM fabricante f WHERE f.pais=(SELECT f.pais FROM fabricante f WHERE f.nombre='oracle');   

-- 42 nombre de aquellos clientes que tienen la misma edad que pepe perez.(subconsulta)
SELECT c.edad FROM cliente c WHERE c.nombre='pepe perez';
SELECT c.nombre FROM cliente c WHERE c.edad=(SELECT c.edad FROM cliente c WHERE c.nombre='pepe perez');  

-- 43 genera un listado con los comercios que tienen su sede en la misma ciudad que tiene el conercio fnac. (subconsulta)
SELECT nombre.c FROM comercio c WHERE c.nombre='fnac';
SELECT c.nombre FROM comercio c WHERE c.nombre=(SELECT c.nombre FROM comercio c where  c.nombre='fnac');  
   
-- 44 nombre de aquellos clientes que han registrado un producto  de la misma forma que el cliente pepe perez (subconsulta)
  -- subconsulta
 SELECT r.medio FROM registra r WHERE dni=(SELECT c.dni FROM cliente c WHERE c.nombre='pepe perez');
-- consulta final
SELECT r.dni, r.medio FROM registra r JOIN (SELECT r.medio FROM registra r 
WHERE dni=(SELECT c.dni FROM cliente c WHERE c.nombre='pepe perez')) c1 ON c1.medio=r.medio;  

-- 45 obtener el numero de programas que hay en la tabla programas
SELECT COUNT(*) FROM programa p ;

-- 46 calcula el numero de clientes cuya edad es mayor de 40 años
SELECT COUNT(*) FROM cliente c WHERE c.edad>'40'; 

-- 47 calcula el numero de productos que ha vendido el establecimiento cuyo cif es 1
SELECT COUNT(*) FROM distribuye d JOIN programa p ON d.codigo = p.codigo WHERE d.cif='1';  

-- 48 calcula la media de programas que se venden cuyo codigo es 7
SELECT AVG( d.cantidad) FROM programa p JOIN distribuye d ON p.codigo = d.codigo WHERE p.codigo='7'; 

-- 49 calcula la minima cantidad de programas de codigo 7 que se ha vendido
SELECT MIN( d.cantidad) FROM programa p JOIN distribuye d ON p.codigo = d.codigo WHERE d.codigo='7'; 

-- 50 calcula la maxima cantidad de programas de codigo 7 que se ha vendido
SELECT MAX( d.cantidad) FROM programa p JOIN distribuye d ON p.codigo = d.codigo WHERE d.codigo='7';

-- 51 ¿en cuantos establecimientos se venden el programa cuyo codigo es 7?
SELECT COUNT(*) FROM comercio c JOIN distribuye d ON c.cif = d.cif WHERE d.codigo='7'; 

-- 52 calcular el numero de registros que se han realizado por intenet
SELECT COUNT(*) FROM registra r WHERE r.medio='internet';  

-- 53 obtener el numero de programas que se han vendido en sevilla
SELECT COUNT(*) FROM distribuye d JOIN programa p ON d.codigo = p.codigo JOIN comercio c ON d.cif = c.cif WHERE c.ciudad='sevilla'; 

-- 54 calcular en numero total de programas que han desarrollado los fabricantes cuyo pais es estados unidos
SELECT * FROM desarrolla d JOIN fabricante f ON d.id_fab = f.id_fab WHERE f.pais='estados unidos';
   
-- 55 visualiza el nombre de todos los clientes en mayuscula. en el resultado de la consulta debe aparecer tambien la longitud de la cadena nombre
SELECT UPPER( c.nombre), LENGTH( c.nombre) FROM cliente c; 

-- 56 con una consulta concatena los campos nombre y version de la tabla programa
  (SELECT p.nombre FROM programa p) UNION (SELECT p.version FROM programa p);  